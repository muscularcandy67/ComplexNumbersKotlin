import java.lang.Math.*

class ComplexNumber (private var r:Double, private var i:Double) {

    fun getR() = this.r

    fun getI() = this.i

    fun conj() = ComplexNumber(r, -i)

    fun add(z: ComplexNumber){
        this.r.plus(z.getR())
        this.i.plus(z.getI())
    }

    fun minus(z: ComplexNumber){
        this.r.minus(z.getR())
        this.r.minus(z.getI())
    }

    fun absSquared():Double=pow(r,2.0) + pow(i,2.0)

    fun abs():Double = abs(absSquared())

    fun neg(){
        this.r=-this.r
        this.i=-this.i
    }

    fun mult(z: ComplexNumber)= ComplexNumber(
            this.r*z.getR()-this.i*z.getI(),
            this.r*z.getI()-this.r*z.getR()
    )

    fun inv(): ComplexNumber {
        val absSquared=absSquared()
        return ComplexNumber(
                this.r/absSquared,
                this.i/absSquared
        )
    }

    fun div(z:ComplexNumber)=this.mult(z=z.inv())

    fun div(z: Double) = {
        this.r = this.r / z
        this.i = this.i / z
    }

    fun exp() = ComplexNumber(Math.exp(this.r)*Math.cos(this.i), Math.exp(this.r)*Math.sin(this.i))

}